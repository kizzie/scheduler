package com.kat.Model;

/**
 * Created by kat on 23/08/2016.
 */
public class Course {

    private String courseID;
    private String name;

    public Course(String courseID, String name) {
        this.courseID = courseID;
        this.name = name;
    }

    public String getCourseID() {
        return courseID;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseID='" + courseID + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        if (courseID != null ? !courseID.equals(course.courseID) : course.courseID != null) return false;
        return name != null ? name.equals(course.name) : course.name == null;

    }

    @Override
    public int hashCode() {
        int result = courseID != null ? courseID.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
