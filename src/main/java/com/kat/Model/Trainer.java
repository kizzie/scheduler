package com.kat.Model;

import java.util.ArrayList;

/**
 * Created by kat on 23/08/2016.
 */
public class Trainer {

    private String name;
    private Location homeLocation;
    private ArrayList<String> coursesCanTeach;

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    private boolean assigned;

    //constructors for name, location and courses can teach
    public Trainer(String name, Location homeLocation, ArrayList<String> coursesCanTeach) {
        this.name = name;
        this.homeLocation = homeLocation;
        this.coursesCanTeach = coursesCanTeach;
    }

    public Trainer(String name, Location homeLocation) {
        this(name, homeLocation, new ArrayList<String>());
    }

    public Trainer() {
        this("", new Location(""), new ArrayList<String>());
    }

    //getters and setters
    public String getName() {
        return name;
    }

    public Location getHomeLocation() {
        return homeLocation;
    }

    public ArrayList<String> getCoursesCanTeach() {
        return coursesCanTeach;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHomeLocation(Location homeLocation) {
        this.homeLocation = homeLocation;
    }

    public void setCoursesCanTeach(ArrayList<String> coursesCanTeach) {
        this.coursesCanTeach = coursesCanTeach;
    }

    public void addCourse(String courseID){
        this.coursesCanTeach.add(courseID);
    }

    public boolean canTeachCourse(String courseID){
        return coursesCanTeach.contains(courseID);
    }

    //tostring
    @Override
    public String toString() {
        return "Trainer{" +
                "name='" + name + '\'' +
                ", homeLocation=" + homeLocation +
                ", coursesCanTeach=" + coursesCanTeach +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trainer trainer = (Trainer) o;

        if (name != null ? !name.equals(trainer.name) : trainer.name != null) return false;
        if (homeLocation != null ? !homeLocation.equals(trainer.homeLocation) : trainer.homeLocation != null)
            return false;
        return coursesCanTeach != null ? coursesCanTeach.equals(trainer.coursesCanTeach) : trainer.coursesCanTeach == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (homeLocation != null ? homeLocation.hashCode() : 0);
        result = 31 * result + (coursesCanTeach != null ? coursesCanTeach.hashCode() : 0);
        return result;
    }
}
