package com.kat.Model;

/**
 * Created by kat on 23/08/2016.
 */
public class Location {

    private String locationName;

    public Location(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    @Override
    public String toString() {
        return "com.kat.Model.Location{" +
                "locationName='" + locationName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        return locationName != null ? locationName.equals(location.locationName) : location.locationName == null;

    }

    @Override
    public int hashCode() {
        return locationName != null ? locationName.hashCode() : 0;
    }
}
