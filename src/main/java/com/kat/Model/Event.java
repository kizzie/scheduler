package com.kat.Model;

/**
 * Created by kat on 23/08/2016.
 */
public class Event {

    private Location location;
    private Course course;
    private Trainer trainer;

    public Event(Location l, Course c) {
        this.location = l;
        this.course = c;
    }

    public Location getLocation() {
        return location;
    }

    public Course getCourse() {
        return course;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    @Override
    public String toString() {
        return "Event{" +
                "location=" + location +
                ", course=" + course +
                ", trainer=" + trainer +
                '}';
    }

    public boolean hasTrainerSet(){
        return trainer != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (location != null ? !location.equals(event.location) : event.location != null) return false;
        if (course != null ? !course.equals(event.course) : event.course != null) return false;
        return trainer != null ? trainer.equals(event.trainer) : event.trainer == null;

    }

    @Override
    public int hashCode() {
        int result = location != null ? location.hashCode() : 0;
        result = 31 * result + (course != null ? course.hashCode() : 0);
        result = 31 * result + (trainer != null ? trainer.hashCode() : 0);
        return result;
    }
}
