package com.kat;

import com.kat.Model.Event;

import java.util.ArrayList;

/**
 * Created by kat on 23/08/2016.
 */
public class Main {

    public static void main(String[] args){

        int maxNumberOfRounds = 100;
        int numberOfRoundsNotMoved = 0;
        int maxNumberOfRoundsForConvergence = 10;
        boolean debug = true;


        Scheduler r = new Scheduler(debug, maxNumberOfRounds, numberOfRoundsNotMoved, maxNumberOfRoundsForConvergence);
        ArrayList<Event> start = r.initialAssignment();

        r.runHillClimber(start);

//        System.out.println(r.getFinalScore());
//        ArrayList<Event> output = r.getOutputEventList();
//
//        for (Event e : output) {
//            System.out.println(e);
//        }

    }
}
