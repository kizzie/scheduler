package com.kat;

import com.kat.Model.Course;
import com.kat.Model.Event;
import com.kat.Model.Location;
import com.kat.Model.Trainer;
import sun.org.mozilla.javascript.internal.ast.Assignment;

import java.util.ArrayList;

/**
 * Created by kat on 23/08/2016.
 */
public class Scheduler {

    //system
    private ArrayList<Trainer> trainerList;
    private ArrayList<Course> courseList;
    private ArrayList<Location> locationList;
    private ArrayList<Event> initialEventList;

    //setup
    private int maxNumberOfRounds;
    private int numberOfRoundsNotMoved;
    private int maxNumberOfRoundsForConvergence;
    private boolean debug;

    //output
    private ArrayList<Event> outputEventList;
    private int finalScore;

    //constructor
    public Scheduler(boolean debug, int maxNumberOfRounds, int numberOfRoundsNotMoved, int maxNumberOfRoundsForConvergence) {
        initialiseSystem();

        this.debug = debug;
        this.maxNumberOfRounds = maxNumberOfRounds;
        this.numberOfRoundsNotMoved = numberOfRoundsNotMoved;
        this.maxNumberOfRoundsForConvergence = maxNumberOfRoundsForConvergence;
    }

    private void initialiseSystem() {

        if (debug) {
            System.out.println("Initialising system");
        }

        courseList = new ArrayList<Course>();
        courseList.add(new Course("DEVOPPRAC", "DevOps Practitioner"));
        courseList.add(new Course("AIINT", "Intro to AI"));
        courseList.add(new Course("SQL", "Intro to SQL"));
        courseList.add(new Course("WORD1", "Intro to Word"));

        locationList = new ArrayList<Location>();
        locationList.add(new Location("Birmingham"));
        locationList.add(new Location("London"));
        locationList.add(new Location("Manchester"));
        locationList.add(new Location("Edinburgh"));

        trainerList = new ArrayList<Trainer>();

        ArrayList<String> course1 = new ArrayList<String>();
        course1.add("DEVOPPRAC");
        course1.add("AIINT");
        course1.add("SQL");

        ArrayList<String> course2 = new ArrayList<String>();
        course2.add("DEVOPPRAC");
        course2.add("WORD1");

        trainerList.add(new Trainer("Alice", locationList.get(0), course1));
        trainerList.add(new Trainer("Bob", locationList.get(1), course2));

        initialEventList = new ArrayList<Event>();
        initialEventList.add(new Event(locationList.get(1), courseList.get(0)));  //London devopprac
        initialEventList.add(new Event(locationList.get(0), courseList.get(3)));  //Birmingham Word

    }

    public ArrayList<Event> initialAssignment() {
        ArrayList<Event> assignedList = new ArrayList<Event>();
        ArrayList<Trainer> trainersLeft = trainerList;

        //initial pass
        //go through each event
        for (Event e : initialEventList) {

            if (debug) System.out.println("Event: " + e);
            boolean filled = false;
            //find a trainer local that can teach the course and assign them
            for (Trainer t : trainersLeft) {
                if (!t.isAssigned()) {
                    if (debug) System.out.println("Trainer: " + t);
                    if (!filled) {
                        //if the location of the event is the same as the home location
                        if (e.getLocation().equals(t.getHomeLocation())) {
                            if (debug) System.out.println("Home location same");
                            if (t.canTeachCourse(e.getCourse().getCourseID())) {
                                if (debug) System.out.println("and can teach, assigning");
                                t.setAssigned(true);
                                e.setTrainer(t);
                                filled = true;
                                assignedList.add(e);
                            }
                        }
                    } else {
                        break;
                    }
                }
            }
        }

        if (debug) System.out.println("Second pass");
        //second pass - assign anyone left to any course they can teach
        for (Event e : initialEventList) {
            boolean filled = false;

            if (debug) System.out.println("Event: " + e);
            //find a trainer that can teach the course and assign them
            if (!e.hasTrainerSet()) {
                for (Trainer t : trainersLeft) {
                    if (!t.isAssigned()) {
                        if (debug) System.out.println("Trainer: " + t);
                        if (!filled) {
                            if (t.canTeachCourse(e.getCourse().getCourseID())) {
                                if (debug) System.out.println("Can teach, assigning");
                                e.setTrainer(t);
                                t.setAssigned(true);
                                filled = true;
                                assignedList.add(e);
                            }
                        } else {
                            break;
                        }
                    }
                }
            }
        }

        if (debug) {
            System.out.println("end of assignment");
            System.out.println("courses left in list: " + initialEventList.size());
            System.out.println("Trainers left: " + trainersLeft.size());
            System.out.println("assignedList:");
            for (Event e : assignedList) {
                System.out.println(e);
            }

            System.out.println("adding unfilled courses");
        }

        for (Event e : initialEventList){
            if (!e.hasTrainerSet()){
                assignedList.add(e);
            }
        }
        return assignedList;

    }

    public int score(ArrayList<Event> list) {
        return -1;
    }

    public ArrayList<Event> mutate(ArrayList<Event> list) {
        return null;
    }

    public void runHillClimber(ArrayList<Event> startPoint) {
        boolean converged = false;
        int previousBest = 0;
        ArrayList<Event> currentList = startPoint;

        for (int i = 0; i < maxNumberOfRounds; i++) {
            if (!converged) {
                ArrayList<Event> newList = mutate(currentList);
                int newScore = score(newList);

                if (newScore > previousBest) {
                    currentList = newList;
                    previousBest = newScore;
                    numberOfRoundsNotMoved = 0;
                } else {
                    numberOfRoundsNotMoved++;
                    if (numberOfRoundsNotMoved >= maxNumberOfRoundsForConvergence) {
                        converged = true;
                    }
                }
            } else {
                break;
            }
        }

        outputEventList = currentList;
        finalScore = previousBest;
    }

    public ArrayList<Event> getOutputEventList() {
        return outputEventList;
    }

    public int getFinalScore() {
        return finalScore;
    }
}

