/**
  * Created by kat on 09/11/2016.
  */
package scheduler {



  case class course(name: String)
case class person(name: String, coursesCanTeach : List[course])
case class env(var filledCourses : Map[course, person], var unfilledCourses : List[course], var allTrainers : List[person])

object runner extends App {
  val a = course("Java")
  val b = course("Docker")
  val c = course("SQL")
  val d = course("Scala")

  val courseList = List(a, b, c)
  val personList = List(
    person("Alice", List(a, b)),
    person("Bob", List(b, c)),
    person("Eve", List(a))
  )

  val startPoint = env(Map(), List(c, a, b), personList)

  val hc = new hillclimber(startPoint)

  println(hc.score(startPoint))

  hc.runHillClimber()
}

class hillclimber (startPoint : env, maxIterations : Int = 10, timesStayed : Int = 100)
{

  def runHillClimber()  {
    //hill climber algorithm
    //assign things randomly to start
    var oldModel = generateStart(startPoint)
    var currentBestScore = score(oldModel)
    var numberSinceLastChange = 0

    println("going into loop")
    //after x iterations or y where there was no movement finish
    for (x <- 0 to maxIterations if numberSinceLastChange < timesStayed) {
        println(x)
        //mutate
        val newModel = mutate(oldModel)
        //score model again
        val newScore = score(oldModel)
        println("New Score: " + newScore + " Current Best Score: " + currentBestScore)
        //if better, mutate new, else mutate old
        if (newScore >= currentBestScore) {
          println("Using New Model")
          oldModel = newModel
          currentBestScore = newScore
          numberSinceLastChange = 0
        } else {
          println("Sticking with old")
          numberSinceLastChange += 1
        }
    }

    println("End state")
    println(oldModel)
    println("End score")
    println(currentBestScore)

  }


  /*
  This needs to randomly assign people to courses
  and then send back the final environment for the start
  of the algorithm
   */
  def generateStart(e : env) : env = {
    val randomised = e.copy()

    println(e.unfilledCourses)

    //for each course in the list
    for (c <- e.unfilledCourses) {
      var filled = false
      //find the first trainer who can teach it and add them to it (and if it hasn't been sorted already!)
      for (t <- randomised.allTrainers if t.coursesCanTeach.contains(c) if ! filled) {
        //assign them
        randomised.filledCourses = randomised.filledCourses + ((c, t))
        //remove them from the trainer list (?)
        randomised.allTrainers = removeElem(randomised.allTrainers, t)
        randomised.unfilledCourses = removeElem(randomised.unfilledCourses, c)

        filled = true

      }
      //if there isn't anyone available, leave it and move on to the next
    }

    //sort out any left into a random empty course
    for (c <- randomised.unfilledCourses) {
      var filled = false
      for (t <- randomised.allTrainers if ! filled) {
        randomised.filledCourses = randomised.filledCourses + ((c, t))
        randomised.allTrainers = removeElem(randomised.allTrainers, t)
        randomised.unfilledCourses = removeElem(randomised.unfilledCourses, c)
        filled = true
      }
    }

    //return the final state
    println(randomised)
    randomised
  }


  def removeElem[A](l: List[A], e: A): List[A] = {
    val split = l.splitAt(l.indexOf(e))
    split._1 ++ split._2.tail
  }

  /*
  This needs to take two trainers and swap them
   */
  def mutate(e: env): env = {

    var eout = Map[course, person]()
    //go through x times, swap someone on the unassigned list for a course
    //they can teach
    import scala.util.Random
    val r = new Random()

    var a = 0
    var b = 0

    if (e.filledCourses.size > 1) {
      //get two different values
      while (a == b) {
        a = r.nextInt(e.filledCourses.size)
        b = r.nextInt(e.filledCourses.size)
      }

      val c1 = e.filledCourses.keySet.toList(a)
      val c2 = e.filledCourses.keySet.toList(b)

      val p1 = e.filledCourses(c1)
      val p2 = e.filledCourses(c2)

      eout = e.filledCourses + ((c1, p2)) + ((c2, p1))

    }

    e.filledCourses = eout
    e
  }

  def score(e : env): Int = {
    //for each filled course add 1
    //for each unfilled course remove 1
    //if the trainer can actually teach the course add 10
    //otherwise take away 10
    var x = e.filledCourses.size - e.unfilledCourses.size

    for (c <- e.filledCourses) {
      if (c._2.coursesCanTeach.contains(c._1)) {
        //println("adding 10" + c._2 + c._1)
        x += 10
      } else {
        //println("removing 10")
        x -= 10
      }
    }

    x
  }
}
}
