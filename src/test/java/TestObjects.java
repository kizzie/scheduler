import com.kat.Model.Course;
import com.kat.Model.Event;
import com.kat.Model.Location;
import com.kat.Model.Trainer;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by kat on 23/08/2016.
 */
public class TestObjects {


        private Course c;
        private Location l;
        private Trainer t;
        private Event e;

        @Before
        public void setupClasses(){
            c = new Course("QADEVOPPRAC", "DevOps Practitioner");
            l = new Location("Birmingham");
            t = new Trainer("Name", l);
            t.addCourse("DEVOPPRAC");
            t.addCourse("AIINT");
            e = new Event(l, c);

        }

        @Test
        public void trainerCourseContains(){
            assertTrue(t.canTeachCourse("AIINT"));
        }

        @Test
        public void eventHasTrainerSet(){
            assertFalse(e.hasTrainerSet());
            e.setTrainer(t);
            assertTrue(e.hasTrainerSet());
        }


}
