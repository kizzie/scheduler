/**
  * Created by kat on 09/11/2016.
  */
package scheduler {

import org.scalatest._

 class RunTests extends FlatSpec with Matchers{

  object setup {
    //inital setup
    val a = course("Java")
    val b = course("Docker")
    val c = course("SQL")

    val courseList = List(a, b, c)
    val personList = List(
      person("Alice", List(a, b)),
      person("Bob", List(b, c)),
      person("Eve", List(c))
    )

    val startPoint = env(Map(), List(a, b, c), personList)
  }

    "The Hill Climber Score Method" should
      "Return -3 when given the initial test env" in {
      val hc = new hillclimber(setup.startPoint)
      hc.score(setup.startPoint) should be (-3)
    }

    it should "return 33 when given a completed schedule" in {
      val goodCombo = env(Map(
        (setup.a, setup.personList(0)),
        (setup.b, setup.personList(1)),
        (setup.c, setup.personList(2))
      ),
      List(), List())
      val hc = new hillclimber(goodCombo)
      hc.score(goodCombo) should be (33)
    }

    it should "return 13 when given a slightly wrong schedule" in {
      val goodCombo = env(Map(
        (setup.a, setup.personList(0)),
        (setup.b, setup.personList(2)),
        (setup.c, setup.personList(1))
      ),
      List(), List())

      val hc = new hillclimber(goodCombo)
      hc.score(goodCombo) should be (13)
    }

   "RemoveElem" should "remove the element of a given list" in {
      val hc = new hillclimber(setup.startPoint)

     val list = List(1,2,3,4,5,6)
     hc.removeElem(list, 4) should contain allOf (1,2,3,5,6)
   }

   it should "also work with lists of objects" in {
     val hc = new hillclimber(setup.startPoint)
     val list2 = List(person("Alice", List()), person("Bob", List()))

     list2.size should be (2)
     list2.contains(person("Alice", List())) should be (true)

     val newlist = hc.removeElem(list2, person("Alice", List()))

     newlist should contain only (person("Bob", List()))
     newlist.size should be (1)

   }

   "Generate Start" should "Assign trainers to courses as it finds them" in {
     val hc = new hillclimber(setup.startPoint)

     val e = hc.generateStart(setup.startPoint)

     e.filledCourses.size should be (3)
     e.unfilledCourses.size should be (0)
     e.allTrainers.size should be (0)
   }

   it should "leave one course with no trainer in the alternate setup" in {
     val a = course("Java")
     val b = course("Docker")
     val c = course("SQL")
     val d = course("Scala")

     val courseList = List(a, b, c)
     val personList = List(
       person("Alice", List(a, b)),
       person("Bob", List(b, c)),
       person("Eve", List(c))
     )

     val startPoint = env(Map(), List(d, a, b, c), personList)

     val e = new hillclimber(startPoint).generateStart(startPoint)

     e.filledCourses.size should be (3)
     e.unfilledCourses.size should be (1)
     e.allTrainers.size should be (0)
   }

   it should "Assign all the courses even when one doesn't have a match" in {
     val a = course("Java")
     val b = course("Docker")
     val c = course("SQL")
     val d = course("Scala")


     val personList = List(
       person("Alice", List(a, b)),
       person("Bob", List(b, c)),
       person("Eve", List(a))
     )

     val startPoint = env(Map(), List(d, a, b), personList)

     val e = new hillclimber(startPoint).generateStart(startPoint)

     e.filledCourses.size should be (3)
     e.unfilledCourses.size should be (0)
     e.allTrainers.size should be (0)
   }
 }






}
